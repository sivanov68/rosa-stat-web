$(document).ready(function () {

    var json = new Object();
    json['name'] = $('#project').html();
    $.ajax({
        type: "POST",
        url: "getABF",
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //Перед отправкой
        beforeSend: function () {
            $('#id').hide();
            $('#owner').hide();
            $('#desc').hide();
            $('#preloader').show();
        },
        //После успешного завершения
        success: function (data) {
            var arr = data['data']
            if (arr == "Error") {
                $('#id').html("Id: Недоступно<br/>");
                $('#owner').html("Владелец: Недоступно<br/>");
                $('#desc').html("Описание: Недоступно");
            } else {
                $('#id').html("Id: " + arr[0] + "<br/>");
                $('#owner').html("Владелец: " + arr[1] + "<br/>");
                $('#desc').html("Описание: " + arr[2]);
            }
            $('#preloader').hide();
            $('#id').show();
            $('#owner').show();
            $('#desc').show();
        }
    });

    if (parseInt($('#commaCount').html()) > 1) {
        require([
            "dojox/charting/Chart",
            "dojox/charting/themes/Bahamation",
            "dojox/charting/plot2d/Lines",
            "dojox/charting/action2d/MouseZoomAndPan",
            "dojox/charting/widget/Legend",
            "dojox/charting/action2d/Tooltip",
            "dojox/charting/action2d/Magnify",
            "dojox/charting/action2d/MouseIndicator",
            "dojo/fx/easing",
            "dojox/charting/plot2d/Markers",
            "dojox/charting/axis2d/Default",
            "dojo/domReady!"
        ], function (Chart, theme, PlotLines, MouseZoomAndPan, Legend, Tooltip, Magnify, MouseIndicator, easing) {
            var chartData = JSON.parse($('#data').val().replace(/'/g, "\""));
            var chart = new Chart("chartNode");
            chart.setTheme(theme);

            chart.addPlot("default", {
                type: PlotLines,
                markers: false,
                //interpolate:true,
                tension: 3
                //animate: { duration: 1000, easing: easing.elasticOut 	 }
            });

            var myLabelFunc = function (text, value, precision) {
                if (value == 0) return "Dec 10";

                if (value <= 21)
                    return "Dec " + (value + 10);
                else if (value <= 51)
                    return "Jan " + (value - 20);
                else if (value <= 79)
                    return "Feb " + (value - 51);
                else if (value <= 110)
                    return "Mar " + (value - 79);
                else if (value <= 140)
                    return "Apr " + (value - 110);
                else if (value <= 171)
                    return "May " + (value - 140);
                else if (value <= 201)
                    return "Jun " + (value - 171);
                else if (value <= 232)
                    return "Jul " + (value - 201);
                else if (value <= 263)
                    return "Aug " + (value - 232);
                else if (value <= 293)
                    return "Sep " + (value - 263);
                else if (value <= 324)
                    return "Oct " + (value - 293);
                else if (value <= 344)
                    return "Nov " + (value - 324);
                else if (value <= 365)
                    return "Dec " + (value - 354);
                else return myLabelFunc(text, value - 365, precision);
            };

            chart.addAxis("x", {
                natural: true,
                fixed: true,
                fixLower: "major",
                fixUpper: "major",
                labelFunc: myLabelFunc
            });
            chart.addAxis("y", {
                min: 0,
                vertical: true,
                fixLower: "major",
                fixUpper: "major",
                max: getMaxValue(getSubArrayY(chartData)) + 1
            });
            chart.addSeries("Клики", chartData);

            new MouseZoomAndPan(chart, "default", {
                axis: "x",
                enableDoubleClickZoom: false,
                keyZoomModifier: 2,
                scaleFactor: 1.4,
                maxScale: getMaxValue(getSubArrayX(chartData)) -
                getMinValue(getSubArrayX(chartData))
            });

            new MouseIndicator(chart, "default", {
                series: "Клики",
                font: "normal normal bold 12pt Tahoma",
                labelFunc: function (v) {
                    return v.y + ", " + myLabelFunc("", v.x, 0);
                }
            });

            chart.render();
        });
    }
});

function getSubArrayY(array) {
    var subArr = new Array();
    for (var i = 0; i < array.length; i++) {
        subArr[i] = (array[i].y);
    }
    return subArr;
}
function getSubArrayX(array) {
    var subArr = new Array();
    for (var i = 0; i < array.length; i++) {
        subArr[i] = (array[i].x);
    }
    return subArr;
}

function getMaxValue(array) {
    var max = array[0]; // берем первый элемент массива
    for (var i = 0; i < array.length; i++) { // переберем весь массив
        // если элемент больше, чем в переменной, то присваиваем его значение переменной
        if (max < array[i]) max = array[i];
    }
    return max;
}

function getMinValue(array) {
    var min = array[0]; // берем первый элемент массива
    for (var i = 0; i < array.length; i++) { // переберем весь массив
        // если элемент больше, чем в переменной, то присваиваем его значение переменной
        if (min > array[i]) min = array[i];
    }
    return min;
}

