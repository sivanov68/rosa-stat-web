$(document).ready(function () {

    $('#preloader').hide();

    //От
    $('#from').datepicker({
        format: "yyyy-mm-dd"
    });
    //Изменение даты
    $('#from').on('changeDate', function (ev) {
        $(this).datepicker('hide');
        $(this).blur();
    });

    //До
    $('#to').datepicker({
        format: "yyyy-mm-dd"
    });
    //Изменение даты
    $('#to').on('changeDate', function (ev) {
        $(this).datepicker('hide');
        $(this).blur();
    });

    //Таблица
    var table = $('#table').dataTable({
        "sDom": 'Rfrtlip',
        "columns": [
            {"width": "50%"},
            {"width": "20%"},
            {"width": "30%"}
        ],
        "language": {
            "sProcessing": "Подождите...",
            "sLengthMenu": "Показать _MENU_ записей",
            "sZeroRecords": "Записи отсутствуют.",
            "sInfo": "Записи с _START_ до _END_ из _TOTAL_ записей",
            "sInfoEmpty": "Записи с 0 до 0 из 0 записей",
            "sInfoFiltered": "(отфильтровано из _MAX_ записей)",
            "sInfoPostFix": "",
            "sSearch": "Поиск:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Первая",
                "sPrevious": "Предыдущая",
                "sNext": "Следующая",
                "sLast": "Последняя"
            },
            "oAria": {
                "sSortAscending": ": активировать для сортировки столбца по возрастанию",
                "sSortDescending": ": активировать для сортировки столбцов по убыванию"
            }
        }
    });

    //Поиск
    $('#find').click(function () {
        var json = new Object();
        json['from'] = $('#from').val().toString();
        json['to'] = $('#to').val().toString();
        if ($('#x86').is(":checked")) {
            json['x86'] = "on"
        } else {
            json['x86'] = "off"
        }
        if ($('#x64').is(":checked")) {
            json['x64'] = "on"
        } else {
            json['x64'] = "off"
        }
        $.ajax({
            type: "POST",
            url: "/RosaStat/find",
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //Перед отправкой
            beforeSend: function () {
                $('#preloader').show();
                $('#table').hide();
            },
            //После успешного завершения
            success: function (data) {
                table.fnClearTable();
                var arr = data['data']
                $.each(arr, function (key, value) {
                    var obj = [];
                    var ie86 = (value[1] == "is86") ? 'x86' : 'x64';
                    obj.push("<a href='package?name=" + value[0] + "&ie86=" + ie86 + "&project=" + value[3] + "'>" + value[0] + "</a>");
                    obj.push(value[1]);
                    obj.push(value[2])
                    table.fnAddData(obj);
                })
                table.fnDraw();
                $('#find').blur();
                $('#preloader').hide();
                $('#table').show();
                //Переход к пакету
            }
        });
    });

    //Очистить
    $('#clean').click(function () {
        $('#from').val($('#startH').val());
        $('#from').datepicker('update');
        $('#to').val($('#endH').val());
        $('#to').datepicker('update');
        $('#x86').prop("checked", true);
        $('#x64').prop("checked", true);
        $(this).blur();
    });
});