<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>РОСА-СТАТ</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap/bootstrap.css"/>"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap/datepicker.css"/>"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/dataTables.bootstrap.css"/>"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/main.css"/>" media="screen"/>

    <link href="<c:url value="/resources/images/favicon.ico"/>" rel="shortcut icon" type="image/vnd.microsoft.icon" />

    <script src="<c:url value="/resources/js/jquery/jquery-2.1.1.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap/bootstrap-datepicker.js"/>"></script>
    <script src="<c:url value="/resources/js/dataTable/jquery.dataTables.js"/>"></script>
    <script src="<c:url value="/resources/js/dataTable/dataTables.bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/main/index.js"/>"></script>
</head>
<body>
<h2 align="center" style="margin-top:24px;"><a href="<c:url value="/"/>">РОСА-СТАТ</a></h2>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Основная информация</h3>
    </div>
    <div class="panel-body">
        Дата последнего обновления: ${end}<br>
        Продолжительность последнего обновления: ${timeString}<br>
        Сбор статистики проводится в автоматическом режиме ежедневно в 00:00 MSK
    </div>
</div>
<div class="container" style="width:700px;">
    <div class="row">
        <div class="col-xs-3">
            <h5 align="left">Временной период:</h5>
        </div>
        <div class="col-xs-4">
            <div class="input-group">
                <span class="input-group-addon">С</span>
                <input id="from" type="text" class="form-control" value="${start}">
            </div>
        </div>
        <div class="col-xs-4">
            <div class="input-group">
                <span class="input-group-addon">По</span>
                <input id="to" type="text" class="form-control" value="${end}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <h5 align="left">Архитектура:</h5>
        </div>
        <div class="col-xs-4" align="right">
            <label class="checkbox">
                <input id="x64" type="checkbox" checked> i586
            </label>
        </div>
        <div class="col-xs-4"  style="padding-left:35px;">
            <label class="checkbox">
                <input id="x86" type="checkbox" checked> x86_64
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" align="center" style="margin-left: 8.3%">
            <div class="btn-group" role="group" aria-label="...">
                <button id="find" type="button" class="btn btn-default">Показать</button>
                <button id="clean" type="button" class="btn btn-default">Очистить</button>
            </div>
        </div>
    </div>
</div>
<hr/>
<div id="preloader" class="text-center">
    <img src="<c:url value="/resources/images/preloader.GIF"/>"/>
</div>
<table id="table" class="display" class="table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Название</th>
        <th>Архитектура</th>
        <th>Кол-во</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${packages}" var="p">
        <tr>
            <th> <a href="package?name=${p.name}&ie86=${p.is86 ? 'x86' : 'x64'}&project=${p.projectName}"><c:out value="${p.name}"/></a></th>
            <th><c:out value="${p.is86 ? 'x86_64' : 'i586'}"/></th>
            <th><c:out value="${p.count}"/></th>
        </tr>
    </c:forEach>
    </tbody>
</table>
<input id="startH" type="hidden" value="${start}"/>
<input id="endH" type="hidden" value="${end}"/>
</body>
</html>
