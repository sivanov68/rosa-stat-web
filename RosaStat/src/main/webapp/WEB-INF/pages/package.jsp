<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>РОСА-СТАТ</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap/bootstrap.css"/>"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap/datepicker.css"/>"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/jquery.dataTables.css"/>"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dataTable/dataTables.bootstrap.css"/>"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/main.css"/>" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/package.css"/>" media="screen"/>

    <link href="<c:url value="/resources/images/favicon.ico"/>" rel="shortcut icon" type="image/vnd.microsoft.icon"/>

    <script src="<c:url value="/resources/js/jquery/jquery-2.1.1.js"/>"></script>
    <script src="<c:url value="/resources/dojo/dojo/dojo.js"/>"></script>
    <script src="<c:url value="/resources/js/main/package.js"/>"></script>

</head>
<body>
<div class="row">
    <div class="col-xs-1">
        <div class="btn-group btn-group-lg" role="group" style="margin-bottom:10px; margin-top:1px;" aria-label="...">
            <a role="button" type="button" title="Назад" href="/RosaStat" class="btn btn-default" aria-label="Назад">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
            </a>
        </div>
    </div>
    <div class="col-xs-11">
        <h2 align="center" style="margin-right:9.25%;margin-bottom:10px; margin-top:5px;"><a href="<c:url value="/"/>">РОСА-СТАТ</a>
        </h2>
    </div>

</div>

<div class="row" style="height:150px;">
    <div class="col-xs-6">
        <div class="panel panel-default" style="overflow-y:auto; ">
            <div class=" panel panel-heading" style="margin-bottom:5px;">
                Информация о пакете
            </div>
            <div style="padding: 1%;height:85px;">
                <span id="commaCount" hidden="hidden">${commaCount}</span>
                <span id="project" hidden="hidden">${project}</span>
                Имя пакета: <span id="name">${name}</span></br>
                Архитектура: <span id="ie86">${(ie86 eq 'x86') ? 'x86_64' : 'i586'}</span></br>
                Ссылка на ABF: <a href="https://abf.io/import/${name}" target="_blank">https://abf.io/import/${name}</a>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="panel panel-default" style="overflow-y:auto; ">
            <div class=" panel panel-heading" style="margin-bottom:5px;">
                Дополнительная информация предаставлена с <a href="https://abf.rosalinux.ru/" target="_blank">abf.rosalinux.ru</a>
            </div>
            <div style="padding: 1%;height:85px;">
                <div id="preloader" class="text-center">
                    <img src="<c:url value="/resources/images/preloader.GIF"/>"/>
                </div>
                <span id="id"></span>
                <span id="owner"></span>
                <span id="desc"></span>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        График частоты использования
    </div>
    <c:choose>
        <c:when test="${commaCount > 1}">
            <div class="text-center" style="overflow-x:auto;">
                <div id="chartNode" style="width:97%; padding-left:1.5%; overflow:visible;height:400px;">
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div style="height: 400px;">
            <h3 style="padding-top:180px" align="center">Не хватает данных для отображения графика</h3>
            </div>
        </c:otherwise>
    </c:choose>
</div>
<input id="data" type="hidden" value="${data}">
</body>
</html>
