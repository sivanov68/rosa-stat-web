package hse.rosa.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;

@Repository
//Запросы для таблица updates
public class JdbcUpdatesDAO {

    private JdbcTemplate jdbcTemplate;

    /**
     * Дата начала сбора информации
     *
     * @return дата начала
     */
    public Date getStartDate() {
        return this.jdbcTemplate.queryForObject("SELECT Date FROM updates ORDER BY Date LIMIT 1;", Date.class);
    }

    /**
     * Дата окончания сбора информации
     *
     * @return дата окончания
     */
    public Date getEndDate() {
        return this.jdbcTemplate.queryForObject("SELECT Date FROM updates ORDER BY Date DESC LIMIT 1;", Date.class);
    }

    /**
     * Продолжительность последнего обновления
     *
     * @return продолжительность
     */
    public int getEndDuration() {
        return this.jdbcTemplate.queryForObject("SELECT Duration FROM updates ORDER BY Date DESC LIMIT 1;", Integer.class);
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
}
