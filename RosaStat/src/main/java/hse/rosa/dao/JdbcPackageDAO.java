package hse.rosa.dao;

import hse.rosa.classes.SimpleStatResult;
import hse.rosa.mappers.SimpleStatResultMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.List;

@Repository
//Запросы для таблица package
public class JdbcPackageDAO {

    private JdbcTemplate jdbcTemplate;

    /**
     * Получение пакетов по параметрам
     *
     * @param start дата начала
     * @param end   дата окончания
     * @param x64   архитектура
     * @param x86   архитектура
     * @return список пакетов
     */
    public List<SimpleStatResult> getPackages(Date start, Date end, Boolean x64, Boolean x86) {
        return this.jdbcTemplate.query("SELECT p.Name, p.Project, p.Is86, u.Date, SUM(r.Count) AS Count " +
                "FROM package AS p " +
                "LEFT JOIN results AS r ON p.Id = r.PackageId " +
                "LEFT JOIN updates AS u ON u.Id = r.UpdateId " +
                "WHERE DATE(r.StatDate) <= ? AND DATE(r.StatDate) >= ? " +
                (!x64 ? " AND p.Is86=True " : " ") + (!x86 ? " AND p.Is86=False " : " ") +
                "GROUP BY p.Name, p.Is86", new Object[]{end, start}, new SimpleStatResultMapper());
    }

    /**
     * Получение пакета по параметрам
     *
     * @param name    название пакета
     * @param project название проекта
     * @param ie86    архитектура
     * @return пакет
     */
    public List<SimpleStatResult> getPackage(String name, String project, int ie86) {
        return this.jdbcTemplate.query("SELECT p.Name, p.Project, p.Is86, u.Date, sum(r.Count) AS Count " +
                "FROM package AS p " +
                "LEFT JOIN results AS r ON p.Id = r.PackageId " +
                "LEFT JOIN updates AS u ON u.Id = r.UpdateId " +
                "WHERE p.Name = ? AND p.Is86 = ? AND p.Project = ?" +
                "GROUP BY r.StatDate ORDER BY r.StatDate", new Object[]{name, ie86, project}, new SimpleStatResultMapper());
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
}
