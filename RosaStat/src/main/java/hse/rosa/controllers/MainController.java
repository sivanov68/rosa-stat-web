package hse.rosa.controllers;

import hse.rosa.classes.SessionImpl;
import hse.rosa.classes.SimpleStatResult;
import hse.rosa.services.PackageService;
import hse.rosa.services.UpdatesService;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.JsonNodeFactory;
import org.codehaus.jackson.node.ObjectNode;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    PackageService packageService;

    @Autowired
    UpdatesService updatesService;

    /**
     * Главная страница
     *
     * @param request  запрос
     * @param response ответ
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView mainPage(HttpServletRequest request,
                                 HttpServletResponse response) {
        //Главная страница
        ModelAndView indexMV = new ModelAndView("index");
        //Дата начала сбора информации
        Date start = updatesService.getStartDate();
        //Дата окончания сбора информации
        Date end = updatesService.getEndDate();
        //Время затраченное на последнее обновление
        int totalSecs = updatesService.getEndDuration();
        //Часы
        int hours = totalSecs / 3600;
        //Минуты
        int minutes = (totalSecs % 3600) / 60;
        //Секунда
        int seconds = totalSecs % 60;
        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        indexMV.addObject("timeString", timeString);
        indexMV.addObject("start", start);
        indexMV.addObject("end", end);
        indexMV.addObject("packages", packageService.getPackages(start, end, true, true));
        return indexMV;
    }

    /**
     * Страница пакета
     *
     * @param request     запрос
     * @param response    ответ
     * @param name        название пакета
     * @param projectName название проекта
     * @param ie86        архитектура
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/package", method = RequestMethod.GET)
    public ModelAndView packagePage(HttpServletRequest request,
                                    HttpServletResponse response,
                                    @RequestParam(value = "name", required = true) String name,
                                    @RequestParam(value = "project", required = true) String projectName,
                                    @RequestParam(value = "ie86", required = true) String ie86)
            throws IOException {
        ModelAndView indexMV = new ModelAndView("package");
        //Получение статистика для пакета
        List<SimpleStatResult> results = packageService.getPackage(name, projectName, ("x86".equals(ie86)) ? 1 : 0);
        //Дата начала ведения статистика по всем проектам
        LocalDate localDate = new LocalDate(2014, 12, 10);
        //Упаковка статистики в json
        JsonNodeFactory factory = JsonNodeFactory.instance;
        ArrayNode arrayNode = new ArrayNode(factory);
        for (SimpleStatResult result : results) {
            ObjectNode node = new ObjectNode(factory);
            node.put("x", Days.daysBetween(localDate, new LocalDate(result.getDate())).getDays() + 1);
            node.put("y", result.getCount());
            arrayNode.add(node);
        }
        String s = arrayNode.toString().replaceAll("\"", "'");
        indexMV.addObject("data", arrayNode.toString().replaceAll("\"", "'"));
        indexMV.addObject("name", name);
        indexMV.addObject("commaCount", s.length() - s.replace(",", "").length());
        indexMV.addObject("project", projectName);
        indexMV.addObject("ie86", ie86);
        return indexMV;
    }

    /**
     * Поиск на главной странице (ajax)
     *
     * @param json     информация
     * @param request  запрос
     * @param response ответ
     * @return
     * @throws IOException
     * @throws ParseException
     */
    @RequestMapping(value = "/find", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    String findBetweenDates(@RequestBody String json,
                            HttpServletRequest request,
                            HttpServletResponse response) throws IOException, ParseException {
        //Парсим json
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(json);
        String from = rootNode.get("from").asText();
        String to = rootNode.get("to").asText();
        Boolean x86 = rootNode.get("x86").asText().equals("on");
        Boolean x64 = rootNode.get("x64").asText().equals("on");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dateFrom = new Date(format.parse(from).getTime());
        Date dateTo = new Date(format.parse(to).getTime());
        //Подходящие пакеты
        List<SimpleStatResult> results = packageService.getPackages(dateFrom, dateTo, x64, x86);
        //Упаковка результатов в json
        JsonNodeFactory factory = JsonNodeFactory.instance;
        ArrayNode arrayNode = new ArrayNode(factory);
        for (SimpleStatResult result : results) {
            ArrayNode arrayNodeSub = new ArrayNode(factory);
            arrayNodeSub.add(result.getName());
            if (result.isIs86()) {
                arrayNodeSub.add("x86_64");
            } else {
                arrayNodeSub.add("i586");
            }
            arrayNodeSub.add(result.getCount());
            arrayNodeSub.add(result.getProjectName());
            arrayNode.add(arrayNodeSub);
        }
        ObjectNode returnNode = new ObjectNode(factory);
        returnNode.put("data", arrayNode);
        return returnNode.toString();
    }

    /**
     * Получение информации о пакете с abf (ajax)
     *
     * @param json     информация
     * @param request  запрос
     * @param response ответ
     * @return
     * @throws IOException
     * @throws ParseException
     */
    @RequestMapping(value = "/getABF", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    String get(@RequestBody String json,
               HttpServletRequest request,
               HttpServletResponse response) throws IOException, ParseException {
        //Парсим json
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(json);
        String projectName = rootNode.get("name").asText();
        //Логин и пароль
        SessionImpl si = new SessionImpl("Daniil", "300194");
        //Упаковка результата в json
        JsonNodeFactory factory = JsonNodeFactory.instance;
        ObjectNode returnNode = new ObjectNode(factory);
        try {
            //Получение id пакета по проекту и владельцу
            String content = si.requestContent(si.createConnection(projectName));
            objectMapper = new ObjectMapper();
            rootNode = objectMapper.readTree(content).get("project");
            int id = rootNode.get("id").asInt();
            //Получение информации о пакете через id
            content = si.requestContent(si.createConnection(id));
            ArrayNode arrayNode = new ArrayNode(factory);
            rootNode = objectMapper.readTree(content).get("project");
            arrayNode.add(rootNode.get("id").asInt());
            arrayNode.add(rootNode.get("owner").get("name").isNull() ? "Отсутствует" : rootNode.get("owner").get("name").asText());
            arrayNode.add(rootNode.get("description").isNull() ? "Отсутствует" : rootNode.get("description").asText());
            returnNode = new ObjectNode(factory);
            returnNode.put("data", arrayNode);
        } catch (Exception e) {
            returnNode.put("data", "Error");
        }
        return returnNode.toString();
    }
}
