package hse.rosa.classes;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;

//Сессия для получения информации с abf
public class SessionImpl {
    //Логин
    private String username;
    //Пароль
    private String userpass;

    /**
     * Действие аутентефикации
     */
    private void authenticate() {
        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, userpass.toCharArray());
            }
        });
    }

    /*
    Создание Https соединения
    */
    public HttpsURLConnection createConnection(String projectName) throws Exception {
        //Получение id проекта через название и владельца
        boolean error = false;
        //URL для получения информации
        String https = "https://abf.rosalinux.ru/api/v1/projects/get_id.json?name=%s&owner=%s";
        String firstRequest = String.format(https, projectName, "import");
        URL url;
        HttpsURLConnection con = null;
        //Первый запрос с владельцем import
        try {
            url = new URL(firstRequest);
            con = (HttpsURLConnection) url.openConnection();
            con.setAllowUserInteraction(true);
            con.setRequestMethod("GET");
            con.connect();
            int code = con.getResponseCode();
            if (code != 200) {
                throw new Exception("Пакет не найден");
            }
        } catch (Exception ex) {
            //Ошибка
            error = true;
        }
        //Второй запрос с владельцем server
        if (error) {
            firstRequest = String.format(https, projectName, "server");
            try {
                url = new URL(firstRequest);
                con = (HttpsURLConnection) url.openConnection();
                con.setAllowUserInteraction(true);
                con.setRequestMethod("GET");
                con.connect();
                int code = con.getResponseCode();
                if (code != 200) {
                    throw new Exception("Пакет не найден");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return con;
    }

    /*
   Создание Https соединения
   */
    public HttpsURLConnection createConnection(int id) throws Exception {
        //Получение информации о проекте через id проекта
        String https = "https://abf.rosalinux.ru/api/v1/projects/%s.json";
        String firstRequest = String.format(https, id);
        URL url;
        HttpsURLConnection con = null;
        try {
            url = new URL(firstRequest);
            con = (HttpsURLConnection) url.openConnection();
            con.setAllowUserInteraction(true);
            con.setRequestMethod("GET");
            con.connect();
            int code = con.getResponseCode();
            if (code != 200) {
                throw new Exception("Пакет не найден");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return con;
    }

    /*
    Инициализация сессии с указанным именем пользователя и паролем
    задает имя пользователя и пароль для последующих действий
    */
    public SessionImpl(String username, String userpass) {
        this.username = username;
        this.userpass = userpass;
        authenticate();
    }

    /*
    Функция для строкового представления ответа от сервера
    @con - соединение по которому был отправлен запрос
    */
    public String requestContent(HttpsURLConnection con) {
        String content = "";
        if (con != null) {
            try {
                BufferedReader br =
                        new BufferedReader(
                                new InputStreamReader(con.getInputStream()));
                String input;
                while ((input = br.readLine()) != null) {
                    content += input + "\n";
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return content;
    }
}
