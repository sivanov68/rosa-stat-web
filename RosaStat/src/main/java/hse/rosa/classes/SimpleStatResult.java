package hse.rosa.classes;

import java.sql.Date;

//Отображение результата запроса
public class SimpleStatResult {
    //Название пакета
    String Name;
    //Название проекта
    String projectName;
    //Архитектура
    boolean Is86;
    //Кол-во кликов
    int Count;
    //Дата
    Date date;

    public SimpleStatResult() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean isIs86() {
        return Is86;
    }

    public void setIs86(boolean is86) {
        Is86 = is86;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
