package hse.rosa.classes;

//Пакет
public class Package {
    //id
    int id;
    //Название
    String name;
    //Архитектура
    boolean ie86;

    public Package() {
    }

    public Package(int id, String name, boolean ie86) {
        this.id = id;
        this.name = name;
        this.ie86 = ie86;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIe86() {
        return ie86;
    }

    public void setIe86(boolean ie86) {
        this.ie86 = ie86;
    }
}
