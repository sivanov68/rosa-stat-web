package hse.rosa.mappers;

import hse.rosa.classes.SimpleStatResult;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

//Класс для маппинга полученныз результатов из БД
public class SimpleStatResultMapper implements RowMapper<SimpleStatResult> {

    public SimpleStatResult mapRow(ResultSet rs, int rowNum) throws SQLException {
        SimpleStatResult simpleStatResult = new SimpleStatResult();
        simpleStatResult.setName(rs.getString("Name"));
        simpleStatResult.setProjectName(rs.getString("Project"));
        simpleStatResult.setIs86(rs.getBoolean("Is86"));
        simpleStatResult.setCount(rs.getInt("Count"));
        simpleStatResult.setDate(rs.getDate("Date"));
        return simpleStatResult;
    }
}