package hse.rosa.services;

import hse.rosa.classes.SimpleStatResult;
import hse.rosa.dao.JdbcPackageDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class PackageService {

    @Autowired
    private JdbcPackageDAO jdbcPackageDAO;

    /**
     * Получение пакетов по параметрам
     *
     * @param start дата начала
     * @param end   дата окончания
     * @param x64   архитектура
     * @param x86   архитектура
     * @return список пакетов
     */
    public List<SimpleStatResult> getPackages(Date start, Date end, Boolean x64, Boolean x86) {
        return jdbcPackageDAO.getPackages(start, end, x64, x86);
    }

    /**
     * Получение пакета по параметрам
     *
     * @param name    название пакета
     * @param project название проекта
     * @param ie86    архитектура
     * @return пакет
     */
    public List<SimpleStatResult> getPackage(String name, String project, int ie86) {
        return jdbcPackageDAO.getPackage(name, project, ie86);
    }

}
