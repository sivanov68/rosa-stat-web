package hse.rosa.services;

import hse.rosa.dao.JdbcUpdatesDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class UpdatesService {

    @Autowired
    private JdbcUpdatesDAO jdbcUpdatesDAO;

    /**
     * Дата начала сбора информации
     *
     * @return дата начала
     */
    public Date getStartDate() {
        return jdbcUpdatesDAO.getStartDate();
    }

    /**
     * Дата окончания сбора информации
     *
     * @return дата окончания
     */
    public Date getEndDate() {
        return jdbcUpdatesDAO.getEndDate();
    }

    /**
     * Продолжительность последнего обновления
     *
     * @return продолжительность
     */
    public int getEndDuration() {
        return jdbcUpdatesDAO.getEndDuration();
    }

}
